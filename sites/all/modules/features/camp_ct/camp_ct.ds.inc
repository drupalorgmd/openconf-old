<?php
/**
 * @file
 * camp_ct.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function camp_ct_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'community_front_page';
  $ds_view_mode->label = 'Community Front Page';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['community_front_page'] = $ds_view_mode;

  return $export;
}
