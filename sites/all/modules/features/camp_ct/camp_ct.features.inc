<?php
/**
 * @file
 * camp_ct.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function camp_ct_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function camp_ct_node_info() {
  $items = array(
    'accommodation' => array(
      'name' => t('Accommodation'),
      'base' => 'node_content',
      'description' => t('If you have any places for the attendees to stay, specify them here and they\'ll show up in the <strong>Accommodation</strong> section.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'blog_repost' => array(
      'name' => t('Blog Repost'),
      'base' => 'node_content',
      'description' => t('If you have a mention or a blog article written about the event, and you want to re-post it here, use this content-type for it.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'community' => array(
      'name' => t('Community'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => t('Post news updates to the site using this content type. The default is that they will be shown in a view on /news and the most recent one (or few) will be shown on the home page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'partner' => array(
      'name' => t('Partner'),
      'base' => 'node_content',
      'description' => t('If you have any event partners, this is where you add them.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pricing_table_item' => array(
      'name' => t('Pricing Table Item'),
      'base' => 'node_content',
      'description' => t('If you want to create a Price Package / Pricing Table Item - use this content type. You will be able to specify in which list should it show up.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'room' => array(
      'name' => t('Room'),
      'base' => 'node_content',
      'description' => t('Room is where the track will take place. There can be several rooms and several tracks.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'schedule_item' => array(
      'name' => t('Schedule Item'),
      'base' => 'node_content',
      'description' => t('Whenever you want to post anything into the schedule, you need to create a Schedule Item (unless it\'s a session).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'session' => array(
      'name' => t('Session'),
      'base' => 'node_content',
      'description' => t('Attendees are allowed to add sessions during the Launch phase (2nd phase) by creating nodes of this content-type.'),
      'has_title' => '1',
      'title_label' => t('Session Title'),
      'help' => '',
    ),
    'simple_page' => array(
      'name' => t('Simple Page'),
      'base' => 'node_content',
      'description' => t('If you want to create a simple static page, use this content-type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sponsor' => array(
      'name' => t('Sponsor'),
      'base' => 'node_content',
      'description' => t('If  you have any sponsors for this event, feel free to add them.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'time_slot' => array(
      'name' => t('Time Slot'),
      'base' => 'node_content',
      'description' => t('Time slots are created per room usually, the define which periods of time the room is available. Later schedule items use time slots to know where and when the schedule item takes place.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
