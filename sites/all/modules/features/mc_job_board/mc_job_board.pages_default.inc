<?php
/**
 * @file
 * mc_job_board.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function mc_job_board_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = TRUE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_2';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -29;
  $handler->conf = array(
    'title' => 'News (Disabled)',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'news' => 'news',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecolumn';
  $display->layout_settings = array(
    'use_container' => 1,
    'panel_class' => '',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'one' => array(
        'grid_system' => 'bootstrap',
        'large_grid' => array(
          'grid_large_qty' => '10',
          'grid_large_offset' => '1',
        ),
        'medium_grid' => array(
          'grid_medium_qty' => '12',
          'grid_medium_offset' => '0',
        ),
        'small_grid' => array(
          'grid_small_qty' => '0',
          'grid_small_offset' => '0',
        ),
        'additional_class' => '',
      ),
    ),
    'one' => array(
      'style' => 'columns',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '51be0a6d-20ea-467c-9ce8-ed8f043fcba9';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view_panel_context_2';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-a90b5ea0-0a6a-459a-a875-3b5a54ddb286';
  $pane->panel = 'one';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 0,
    'no_extras' => 0,
    'override_title' => 1,
    'override_title_text' => '',
    'identifier' => '',
    'link' => 0,
    'leave_node_title' => 1,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a90b5ea0-0a6a-459a-a875-3b5a54ddb286';
  $display->content['new-a90b5ea0-0a6a-459a-a875-3b5a54ddb286'] = $pane;
  $display->panels['one'][0] = 'new-a90b5ea0-0a6a-459a-a875-3b5a54ddb286';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_2'] = $handler;

  return $export;
}
