<?php
/**
 * @file
 * mc_job_board.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function mc_job_board_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact_info|node|job|form';
  $field_group->group_name = 'group_contact_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact information',
    'weight' => '1',
    'children' => array(
      0 => 'field_job_address',
      1 => 'field_job_city',
      2 => 'field_job_country',
      3 => 'field_job_email',
      4 => 'field_job_phone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-contact-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_contact_info|node|job|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_job_info|node|job|form';
  $field_group->group_name = 'group_job_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Job information',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_job_salary',
      2 => 'field_key_features',
      3 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-job-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_job_info|node|job|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact information');
  t('Job information');

  return $field_groups;
}
