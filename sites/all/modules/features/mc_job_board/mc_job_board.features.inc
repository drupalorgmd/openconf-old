<?php
/**
 * @file
 * mc_job_board.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mc_job_board_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mc_job_board_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function mc_job_board_node_info() {
  $items = array(
    'job' => array(
      'name' => t('Job position'),
      'base' => 'node_content',
      'description' => t('Job position'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
