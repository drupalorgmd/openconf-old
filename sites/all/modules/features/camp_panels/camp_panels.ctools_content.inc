<?php
/**
 * @file
 * camp_panels.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function camp_panels_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'moldcamp_link_social_icons';
  $content->admin_title = 'Link with Twitter Links';
  $content->admin_description = 'Social icons / links with the link on the left-side.';
  $content->category = 'Moldcamp';
  $content->settings = array(
    'admin_title' => 'Link with Twitter Links',
    'title' => '<none>',
    'body' => '<div class="wrapper">
  <div class="review-link-wrapper">
    <span class="icon fui-video"></span>
    <span>See how it was -<a href="https://www.youtube.com/watch?v=0s8kBCF4Gm4">Moldcamp 2014</a>and<a href="https://www.youtube.com/watch?v=0s8kBCF4Gm4">Moldcamp 2015</a></span>
  </div>
</div>',
    'format' => 'full_html',
    'substitute' => 1,
    'title_heading' => 'h2',
  );
  $export['moldcamp_link_social_icons'] = $content;

  return $export;
}
