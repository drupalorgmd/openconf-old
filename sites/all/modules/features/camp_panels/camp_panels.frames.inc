<?php
/**
 * @file
 * camp_panels.frames.inc
 */

/**
 * Implements hook_default_panels_frame().
 */
function camp_panels_default_panels_frame() {
  $export = array();

  $panels_frame = new stdClass();
  $panels_frame->disabled = FALSE; /* Edit this to true to make a default panels_frame disabled initially */
  $panels_frame->api_version = 1;
  $panels_frame->label = 'Announcement Skeleton';
  $panels_frame->name = 'announcement_skeleton';
  $panels_frame->description = 'This is announcement skeleton (used in panels everywhere).';
  $panels_frame->plugin = 'stack';
  $panels_frame->category = 'Skeletons';
  $panels_frame->data = array(
    'top' => array(
      'label' => 'Top',
      'identifier' => 'top',
      'layout' => 'onecolumn',
      'weight' => 0,
    ),
    'content' => array(
      'label' => 'Content',
      'identifier' => 'content',
      'layout' => 'onecolumn',
      'weight' => 1,
    ),
    'footer' => array(
      'label' => 'Footer',
      'identifier' => 'footer',
      'layout' => 'onecolumn',
      'weight' => 2,
    ),
  );
  $panels_frame->settings = array();
  $export['announcement_skeleton'] = $panels_frame;

  $panels_frame = new stdClass();
  $panels_frame->disabled = FALSE; /* Edit this to true to make a default panels_frame disabled initially */
  $panels_frame->api_version = 1;
  $panels_frame->label = 'Frontpage Layout';
  $panels_frame->name = 'frontpage_layout';
  $panels_frame->description = 'This is used mainly for the frontpage of the launched version.';
  $panels_frame->plugin = 'stack';
  $panels_frame->category = 'Launched';
  $panels_frame->data = array(
    'header' => array(
      'label' => 'Header',
      'identifier' => 'header',
      'layout' => 'onecolumn',
      'weight' => '-10',
    ),
    'info_event' => array(
      'label' => 'Info Event',
      'identifier' => 'info_event',
      'layout' => 'onecolumn',
      'weight' => '-9',
    ),
    'communities' => array(
      'label' => 'Communities',
      'identifier' => 'communities',
      'layout' => 'twocolumn',
      'weight' => '-8',
    ),
    'highlight_sponsors' => array(
      'label' => 'Highlight Sponsors',
      'identifier' => 'highlight_sponsors',
      'layout' => 'twocolumn',
      'weight' => '-7',
    ),
    'gold_sponsors' => array(
      'label' => 'Gold Sponsors',
      'identifier' => 'gold_sponsors',
      'layout' => 'onecolumn',
      'weight' => '-6',
    ),
    'stats' => array(
      'label' => 'Stats',
      'identifier' => 'stats',
      'layout' => 'onecolumn',
      'weight' => '-5',
    ),
    'submitted_speakers' => array(
      'label' => 'Submitted Speakers',
      'identifier' => 'submitted_speakers',
      'layout' => 'onecolumn',
      'weight' => '-4',
    ),
    'front_content' => array(
      'label' => 'Front Content',
      'identifier' => 'front_content',
      'layout' => 'threecolumn',
      'weight' => '-3',
    ),
    'submitted_sessions' => array(
      'label' => 'Submitted Sessions',
      'identifier' => 'submitted_sessions',
      'layout' => 'onecolumn',
      'weight' => '-2',
    ),
    'sponsors_footer' => array(
      'label' => 'Sponsors Footer',
      'identifier' => 'sponsors_footer',
      'layout' => 'onecolumn',
      'weight' => '-1',
    ),
    'location' => array(
      'label' => 'Location',
      'identifier' => 'location',
      'layout' => 'onecolumn',
      'weight' => 0,
    ),
  );
  $panels_frame->settings = array();
  $export['frontpage_layout'] = $panels_frame;

  $panels_frame = new stdClass();
  $panels_frame->disabled = FALSE; /* Edit this to true to make a default panels_frame disabled initially */
  $panels_frame->api_version = 1;
  $panels_frame->label = 'Launched Skeleton';
  $panels_frame->name = 'launched_skeleton';
  $panels_frame->description = 'This is launched skeleton (used in panels everywhere).';
  $panels_frame->plugin = 'stack';
  $panels_frame->category = 'Skeletons';
  $panels_frame->data = array(
    'top' => array(
      'label' => 'Top',
      'identifier' => 'top',
      'layout' => 'onecolumn',
      'weight' => '-10',
    ),
    'breadcrumb' => array(
      'label' => 'Breadcrumb',
      'identifier' => 'breadcrumb',
      'layout' => 'onecolumn',
      'weight' => '-9',
    ),
    'messages_tabs' => array(
      'label' => 'Messages & Tabs',
      'identifier' => 'messages_tabs',
      'layout' => 'onecolumn',
      'weight' => '-8',
    ),
    'content' => array(
      'label' => 'Content',
      'identifier' => 'content',
      'layout' => 'onecolumn',
      'weight' => '-7',
    ),
    'footer' => array(
      'label' => 'Footer',
      'identifier' => 'footer',
      'layout' => 'onecolumn',
      'weight' => '-6',
    ),
  );
  $panels_frame->settings = array();
  $export['launched_skeleton'] = $panels_frame;

  $panels_frame = new stdClass();
  $panels_frame->disabled = FALSE; /* Edit this to true to make a default panels_frame disabled initially */
  $panels_frame->api_version = 1;
  $panels_frame->label = 'One Page Layout';
  $panels_frame->name = 'one_page_layout';
  $panels_frame->description = 'Initial one page layout, used for initial landing / announcement page.';
  $panels_frame->plugin = 'stack';
  $panels_frame->category = 'Announcement';
  $panels_frame->data = array(
    'header' => array(
      'label' => 'Header',
      'identifier' => 'header',
      'layout' => 'onecolumn',
      'weight' => '-10',
    ),
    'highlight_sponsors' => array(
      'label' => 'Highlight Sponsors',
      'identifier' => 'highlight_sponsors',
      'layout' => 'twocolumn',
      'weight' => '-9',
    ),
    'info_event' => array(
      'label' => 'Info Event',
      'identifier' => 'info_event',
      'layout' => 'onecolumn',
      'weight' => '-8',
    ),
    'description' => array(
      'label' => 'Description',
      'identifier' => 'description',
      'layout' => 'onecolumn',
      'weight' => '-7',
    ),
    'info_boxes' => array(
      'label' => 'Info Box',
      'identifier' => 'info_boxes',
      'layout' => 'onecolumn',
      'weight' => '-6',
    ),
    'become_sponsor' => array(
      'label' => 'Become Sponsor',
      'identifier' => 'become_sponsor',
      'layout' => 'onecolumn',
      'weight' => '-5',
    ),
    'communities' => array(
      'label' => 'Communities',
      'identifier' => 'communities',
      'layout' => 'twocolumn',
      'weight' => '-4',
    ),
    'become_a_speaker' => array(
      'label' => 'Become a Speaker',
      'identifier' => 'become_a_speaker',
      'layout' => 'onecolumn',
      'weight' => '-3',
    ),
    'location' => array(
      'label' => 'Location',
      'identifier' => 'location',
      'layout' => 'onecolumn',
      'weight' => '-2',
    ),
  );
  $panels_frame->settings = array();
  $export['one_page_layout'] = $panels_frame;

  $panels_frame = new stdClass();
  $panels_frame->disabled = FALSE; /* Edit this to true to make a default panels_frame disabled initially */
  $panels_frame->api_version = 1;
  $panels_frame->label = 'Three Sections';
  $panels_frame->name = 'three_sections';
  $panels_frame->description = 'Three rows content.';
  $panels_frame->plugin = 'stack';
  $panels_frame->category = 'Launched';
  $panels_frame->data = array(
    'top_section' => array(
      'label' => 'Top Section',
      'identifier' => 'top_section',
      'layout' => 'onecolumn',
      'weight' => 0,
    ),
    'middle_section' => array(
      'label' => 'Middle Section',
      'identifier' => 'middle_section',
      'layout' => 'onecolumn',
      'weight' => 1,
    ),
    'bottom_section' => array(
      'label' => 'Bottom Section',
      'identifier' => 'bottom_section',
      'layout' => 'onecolumn',
      'weight' => 2,
    ),
  );
  $panels_frame->settings = array();
  $export['three_sections'] = $panels_frame;

  $panels_frame = new stdClass();
  $panels_frame->disabled = FALSE; /* Edit this to true to make a default panels_frame disabled initially */
  $panels_frame->api_version = 1;
  $panels_frame->label = 'Two column';
  $panels_frame->name = 'two_column';
  $panels_frame->description = '';
  $panels_frame->plugin = 'stack';
  $panels_frame->category = 'Moldcamp';
  $panels_frame->data = array(
    'two_column_content' => array(
      'label' => 'Two column content',
      'identifier' => 'two_column_content',
      'layout' => 'two-col-content',
      'weight' => 0,
    ),
  );
  $panels_frame->settings = array();
  $export['two_column'] = $panels_frame;

  return $export;
}
