<?php
/**
 * @file
 * camp_panels.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function camp_panels_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ctools_custom_content" && $api == "ctools_content") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_frame" && $api == "frames") {
    return array("version" => "1");
  }
}
