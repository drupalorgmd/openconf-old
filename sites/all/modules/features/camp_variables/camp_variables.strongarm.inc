<?php
/**
 * @file
 * camp_variables.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function camp_variables_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'camp_core_current_phase';
  $strongarm->value = '1';
  $export['camp_core_current_phase'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_camp_node_date';
  $strongarm->value = 'F j, Y g:i a';
  $export['date_format_camp_node_date'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_long';
  $strongarm->value = 'l, F j, Y - H:i';
  $export['date_format_long'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_medium';
  $strongarm->value = 'D, m/d/Y - H:i';
  $export['date_format_medium'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_short';
  $strongarm->value = 'm/d/Y - H:i';
  $export['date_format_short'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_simple_date_month';
  $strongarm->value = 'd F Y';
  $export['date_format_simple_date_month'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'drupal_http_request_fails';
  $strongarm->value = FALSE;
  $export['drupal_http_request_fails'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'drupal_private_key';
  $strongarm->value = 'm0qIOL8Sn3zHiDdGQiLC-bqMwtvHnFRyKXjXyC_Fhe0';
  $export['drupal_private_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_view_modes';
  $strongarm->value = array(
    'fieldable_panels_pane' => array(
      'cta_2_columns' => array(
        'label' => 'CTA 2 columns',
        'custom settings' => 1,
      ),
      'cta_1_column' => array(
        'label' => 'CTA 1 column',
        'custom settings' => 1,
      ),
    ),
  );
  $export['entity_view_modes'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_manager_node_view_disabled';
  $strongarm->value = FALSE;
  $export['page_manager_node_view_disabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panels_everywhere_site_template_enabled';
  $strongarm->value = TRUE;
  $export['panels_everywhere_site_template_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_camp_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 0,
    'logo_path' => 'public://logo.png',
    'logo_upload' => '',
    'default_favicon' => 0,
    'favicon_path' => 'sites/all/themes/camp/favicon.ico',
    'favicon_upload' => '',
    'circle_clearcss' => 1,
    'circle_css_normalize' => 1,
    'circle_css_layout' => 1,
    'circle_css_circlestyles' => 1,
    'circle_css_ie' => 1,
    'circle_css_onefile' => 0,
    'circle_css_bootstrap' => 1,
    'circle_css_bootstrap_version' => '3.3.4',
    'circle_js_bootstrap' => 1,
    'circle_js_bootstrap_version' => '3.3.4',
    'circle_css_bootstrap_local' => 0,
    'circle_js_bootstrap_local' => 0,
    'circle_css_foundation' => 0,
    'circle_css_foundation_version' => '4.3.2',
    'circle_js_foundation' => 0,
    'circle_js_foundation_version' => '4.3.2',
    'circle_css_foundation_local' => 0,
    'circle_js_foundation_local' => 0,
    'circle_modernizr' => 1,
    'circle_modernizr_local' => 0,
    'circle_js_htmlshiv' => 1,
    'circle_js_htmlshiv_local' => 0,
    'circle_js_placeholder' => 1,
    'circle_js_jquerycdn' => 0,
    'circle_js_jquerycdn_version' => '0',
    'circle_footer_js' => 0,
    'circle_js_onefile' => 0,
    'breadcrumb_separator' => ' » ',
    'breadcrumb_append_current' => 0,
    'breadcrumb_hide_single' => 0,
    'humanstxt' => FALSE,
    'circles_enable_less' => 0,
    'circle_css_framework' => 'bootstrap',
    'favicon_mimetype' => 'image/vnd.microsoft.icon',
  );
  $export['theme_camp_settings'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_default';
  $strongarm->value = 'camp2017';
  $export['theme_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_cancel_method';
  $strongarm->value = 'user_cancel_block';
  $export['user_cancel_method'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_email_verification';
  $strongarm->value = 1;
  $export['user_email_verification'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_register';
  $strongarm->value = '0';
  $export['user_register'] = $strongarm;

  return $export;
}
