<div class="special-breadcrumb-wrapper">
  <?php if (isset($breadcrumb)): ?>
    <div class="special-breadcrumb"><?php print $breadcrumb; ?></div>
  <?php endif; ?>

  <?php if (isset($date) || isset($location)): ?>
    <div class="date-location-wrapper">
      <div class="date-location-inner">
        <div class="date-location-cell">
          <div class="date"><?php print $date; ?></div>
          <div class="location"><?php print $location; ?></div>
        </div>
      </div>
    </div>
  <?php endif; ?>
</div>
