<?php

/**
 * @file
 * A drupal template file.
 * 
 * Rendering template for the Contact Form with Map.
 */
?>
<div class="contact-form-map-wrapper">
  <div class="container">
    <div class="top-layer">
      <?php if ($html_before): ?>
        <?php print $html_before; ?>
      <?php endif; ?>
      <div class="contact-form-wrapper">
        <?php print render($contact_form); ?>
      </div>
      <?php if ($html_after): ?>
        <?php print $html_after; ?>
      <?php endif; ?>
    </div>
  </div>
  <div class="background-layer">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2787427.608242928!2d28.389696949999998!3d46.979423999999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c97c3628b769a1%3A0x258119acdf53accb!2sMoldova!5e0!3m2!1sen!2s!4v1427122450234" width="600" height="450" frameborder="0" style="border:0"></iframe>
  </div>
</div>
