<?php

/**
 * @file
 * A drupal template file.
 * 
 * Rendering template for the simple text panes.
 */

?>

<div class="simple-text-wrapper">
  <?php if ($output): ?>
    <?php print $output; ?>
  <?php endif; ?>
</div>
