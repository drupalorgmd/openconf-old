<div class="camp-generic-node-fields">
  <div class="title">
    <?php print $node->title; ?>
  </div>
  <?php if ($node->field_subtitle && !empty($node->field_subtitle)) : ?>
    <div class="subtitle">
      <?php print $node->field_subtitle['und'][0]['value']; ?>
    </div>
  <?php endif; ?>
</div>

