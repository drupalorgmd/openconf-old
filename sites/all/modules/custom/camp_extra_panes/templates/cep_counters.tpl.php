<?php
/*
 * @file
 * Counters.
 */
?>
<div class="counters-wrapper">
  <div class="header-title">
    <h2><?php print $header; ?></h2>
  </div>
  <div class="central-image"></div>
  <div class="counters">
    <?php if ($counters): ?>
      <ul class="inner-counters">
        <?php $count = 1; ?>
        <?php foreach ($counters as $id => $counter): ?>
          <li class="counter-item item-<?php print $count; ?>">
            <span class="label"><?php print $counter['label'] ?></span>
            <span class="<?php print $counter['icon-classes']; ?>"></span>
            <span class="value"><?php print $counter['value'] ?></span>
            <?php $count++; ?>
          </li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  </div>
</div>
