Don't get confused. Follow the rules.

Every content-type (pane) has its own wonderful place where it lives.

Everything is stored in the following folders:
1. Announcement Panes - Panes used *only* in the announcement phase (Phase 1).
2. Pre-Events Panes - Panes used *only* in the pre-event phase (Phase 2).
3. Events Panes - Panes used *only* in the event phase (Phase 3).
4. Post-Events Panes - Panes used *only* in the post-event phase (Phase 4).
5. Generic Panes - Panes used in a generic mode (cross-phases).
