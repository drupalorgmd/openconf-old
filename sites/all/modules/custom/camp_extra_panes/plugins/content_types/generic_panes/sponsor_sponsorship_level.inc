<?php

/**
 * @file
 * This file contains a ctools plugin (content-type).
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Sponsorship level label'),
  'description' => t("Provide sponsor's label block"),
  'category' => t('Camp: Generic'),
  'render callback' => 'sponsor_sponsorship_level_render',
  'edit form' => 'sponsor_sponsorship_level_edit_form',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Edit form for our pane.
 */
function sponsor_sponsorship_level_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Any HTML or plain text.
  $form['sponsor_level_text_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Render Text'),
    '#description' => t('You can use <srtong>[sponsor_level]</srtong> token to define sponsor level.'),
    '#default_value' => isset($conf['sponsor_level_text_value']) ? $conf['sponsor_level_text_value'] : t('Our [sponsor_level] sponsor'),
  );

  // Customize the touch & feel.
  $form['sponsor_level_customize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change Touch & Feel'),
    '#description' => t('If you want to customize the touch & feel, see options below.'),
    '#default_value' => $conf['sponsor_level_customize'],
  );
  $form['sponsor_level_customize_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Touch & Feel'),
    '#dependency' => array('edit-sponsor-level-customize' => array(1)),
  );
  $form['sponsor_level_customize_fieldset']['aligner_start'] = array(
    '#markup' => '<div class="option-text-aligner clearfix">',
  );
  $form['sponsor_level_customize_fieldset']['sponsor_level_heading'] = array(
    '#type' => 'select',
    '#title' => t('Heading Tag'),
    '#default_value' => isset($conf['sponsor_level_heading']) ? $conf['sponsor_level_heading'] : 'h4',
    '#options' => array(
      'h1' => t('h1'),
      'h2' => t('h2'),
      'h3' => t('h3'),
      'h4' => t('h4'),
      'h5' => t('h5'),
      'h6' => t('h6'),
      'div' => t('div'),
      'span' => t('span'),
    ),
  );
    $form['sponsor_level_customize_fieldset']['sponsor_level_align'] = array(
    '#type' => 'select',
    '#title' => t('Text Alignment'),
    '#default_value' => isset($conf['sponsor_level_align']) ? $conf['sponsor_level_align'] : 'center',
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
      'justify' => t('Justify'),
    ),
  );
  $form['sponsor_level_customize_fieldset']['aligner_stop'] = array(
    '#markup' => '</div>',
  );
  return $form;
}

/**
 * Ctools edit form submit handler.
 */
function sponsor_sponsorship_level_edit_form_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Render callback.
 */
function sponsor_sponsorship_level_render($subtype, $conf, $panel_args, $context) {

  if (isset($context->data) && !empty($context->data)) {
    $node_wrapper = entity_metadata_wrapper('node', $context->data);
    $_sponsor_level_term = $node_wrapper->field_sponsorship_level->value();

    if (!empty($_sponsor_level_term)) {
      $vars = array(
        'text' => t(str_replace('[sponsor_level]', $_sponsor_level_term->name, $conf['sponsor_level_text_value'])),
        'heading' => $conf['sponsor_level_heading'],
        'align' => $conf['sponsor_level_align'],
      );

      $block = new stdClass();
      $block->title = '';
      $block->content = theme('sponsorship_level', $vars);

      return $block;
    }
  }
}
