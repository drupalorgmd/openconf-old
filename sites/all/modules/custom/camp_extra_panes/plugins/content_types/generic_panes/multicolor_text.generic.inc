<?php

/**
 * @file
 * This file contains a ctools plugin (content-type).
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Multi-Color Header w/ Text'),
  'description' => t('Multi-Color Header with the Text and an Image.'),
  'category' => t('Camp: Generic'),
  'edit form' => 'camp_extra_panes_multicolor_text_edit_form',
  'render callback' => 'camp_extra_panes_multicolor_text_render',
  'admin info' => 'camp_extra_panes_multicolor_text_admin_info',
  'defaults' => array(
    'cep_include_event_date' => FALSE,
    'cep_include_event_location_value' => '',
    'cep_include_event_date_value' => '',
    'cep_texts_header' => '',
    'cep_texts_description' => '',
    'cep_background_image_style_value' => '',
    'cep_background_image_value' => FALSE,
    'cep_sublogo_html_value' => '',
  ),
);

/**
 * Edit form for our multicolor logo.
 */
function camp_extra_panes_multicolor_text_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Event & Date.
  $form['cep_include_event_date'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show location & date'),
    '#description' => t('Show date and location of the event.'),
    '#default_value' => $conf['cep_include_event_date'],
  );
  $form['cep_include_event_date_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Specify Location & Date values'),
    '#dependency' => array('edit-cep-include-event-date' => array(1)),
  );
  $form['cep_include_event_date_fieldset']['cep_include_event_location_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Location Text'),
    '#description' => t('Please specify the event <strong>location</strong> (usually a city or anything you want to be written in the 1st line).'),
    '#default_value' => $conf['cep_include_event_location_value'],
  );
  $form['cep_include_event_date_fieldset']['cep_include_event_date_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Date Text'),
    '#description' => t('Please specify the event <strong>date</strong> (or anything you want to be written in the 2nd line).'),
    '#default_value' => $conf['cep_include_event_date_value'],
  );

  // Text and header.
  $form['cep_texts_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Texts in the Header indtead of Logo & Mission.'),
  );
  $form['cep_texts_fieldset']['cep_texts_header'] = array(
    '#type' => 'textfield',
    '#title' => t('Large header'),
    '#description' => t("Instead of the LOGO we'll use the header and a subheader. Specify the value if your header here (you can use HTML)."),
    '#default_value' => $conf['cep_texts_header'],
  );
  $form['cep_texts_fieldset']['cep_texts_description'] = array(
    '#type' => 'text_format',
    '#title' => t('Sub-header / Description'),
    '#description' => t('Fill-in the Sub-header / Description. This goes under the header.'),
    '#default_value' => $conf['cep_texts_description']['value'],
    '#format' => $conf['cep_texts_description']['format'],
  );

  // Background Image.
  $all_styles = image_style_options();
  $form['cep_background_image_value'] = array(
    '#type' => 'managed_file',
    '#title' => t('Background Image'),
    '#description' => t('If you want to upload an image to use in the background of the header, feel free.'),
    '#default_value' => $conf['cep_background_image_value'],
    '#upload_location' => 'public://camp/',
  );
  $form['cep_background_image_style_value'] = array(
    '#type' => 'select',
    '#title' => t('Background Image Style'),
    '#description' => t('Which image style to use for the background. Use &lt;none&gt; if you want to use the original size.'),
    '#options' => $all_styles,
    '#default_value' => isset($conf['cep_background_image_style_value']) ? $conf['cep_background_image_style_value'] : '',
  );

  // Any HTML under the logo.
  $form['cep_sublogo_html_value'] = array(
    '#type' => 'text_format',
    '#title' => t('HTML under the Logo'),
    '#description' => t('If you want to use some HTML (link or video or something else) - feel free to use this field.'),
    '#default_value' => $conf['cep_sublogo_html_value']['value'],
    '#format' => $conf['cep_sublogo_html_value']['format'],
  );

  // Append other needed stuff required for the form to work properly.
  form_load_include($form_state, 'inc', 'camp_extra_panes', 'plugins/content_types/generic_panes/multicolor_text.generic');
  return $form;
}


/**
 * Ctools edit form submit handler.
 */
function camp_extra_panes_multicolor_text_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
  // Change status to permanent.
  $file = file_load($form_state['values']['cep_background_image_value']);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);
  // Record that the module (in this example, user module) is using the file.
  file_usage_add($file, 'camp_extra_panes', 'camp', 1);
}

/**
 * Rendering of the multi-color logo pane.
 * 
 * Implements a ctools "render callback" of the $plugin.
 */
function camp_extra_panes_multicolor_text_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $header_vars = array(
    'include_event_date' => $conf['cep_include_event_date'],
    'event_location' => $conf['cep_include_event_location_value'],
    'event_date' => $conf['cep_include_event_date_value'],
    'header_text' => $conf['cep_texts_header'],
    'header_description' => $conf['cep_texts_description'],
    'image' => $conf['cep_background_image_value'],
    'image_style' => $conf['cep_background_image_style_value'],
    'extra_html' => $conf['cep_sublogo_html_value'],
  );
  $block->title = '';
  $block->content = theme('cep_multicolor_header', $header_vars);

  return $block;
}

/**
 * Ctools "admin info" callback for the $plugin.
 */
function camp_extra_panes_multicolor_text_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $block->title = t('Quick overview of Multicolor Header w/ Logo');
  $list = array(
    'type' => 'ul',
    'title' => t('Selected Settings:'),
    'attributes' => array(),
  );
  $list['items'] = _camp_extra_panes_process_conf($conf, '_camp_extra_panes_multicolor_logo_get_settings_value');
  $list_rendered = theme_item_list($list);
  $block->content = $list_rendered;
  return $block;
}

/**
 * Private function that just returns the label of the setting.
 */
function _camp_extra_panes_multicolor_text_get_settings_value($key, $setting = 'label') {
  $mappings = array(
    'cep_include_event_date' => array(
      'label' => t('Show location & date'),
    ),
    'cep_include_event_location_value' => array(
      'label' => t('Location Text'),
    ),
    'cep_include_event_date_value' => array(
      'label' => t('Date Text'),
    ),
    'cep_include_logo_value' => array(
      'label' => t('Use Logo'),
    ),
    'cep_include_logo_link_value' => array(
      'label' => t('Use Logo as a Link'),
    ),
    'cep_include_mission_value' => array(
      'label' => t('Use Mission / Slogan'),
    ),
    'cep_background_image_value' => array(
      'label' => t('Background Image'),
      'type' => 'file',
    ),
    'cep_background_image_style_value' => array(
      'label' => t('Background Image Style'),
    ),
    'cep_sublogo_html_value' => array(
      'label' => t('HTML under the Logo'),
      'type' => 'format',
    ),
  );
  return isset($mappings[$key][$setting]) ? $mappings[$key][$setting] : FALSE;
}
