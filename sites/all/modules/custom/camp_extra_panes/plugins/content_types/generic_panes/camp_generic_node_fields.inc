<?php

/**
 * @file
 * This file contains a ctools plugin (content-type).
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Camp Generic field render'),
  'description' => t("Provide Generic field render block"),
  'category' => t('Camp: Generic'),
  'render callback' => 'camp_generic_node_fields_render',
  'edit form' => 'camp_generic_node_fields_edit_form',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Edit form for our pane.
 */
function camp_generic_node_fields_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];


  // $_fields_data = field_info_instances('node', 'sponsor');

  // foreach ($_fields_data as $key => $value) {
  //   # code...
  // }

  $form['generic_fields_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Generic field setup'),
  );

  $form['generic_fields_fieldset']['generic_node_fields'] = array(
    '#type' => 'select',
    '#title' => t('Field listing'),
    '#default_value' => isset($conf['generic_node_fields']) ? $conf['generic_node_fields'] : '',
    '#multiple' => TRUE,
    '#options' => array('' => 'To be updated with the field list.'),
  );

  return $form;
}

/**
 * Ctools edit form submit handler.
 */
function camp_generic_node_fields_edit_form_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Render callback.
 */
function camp_generic_node_fields_render($subtype, $conf, $panel_args, $context) {

  if (isset($context->data) && !empty($context->data)) {
    $vars = array(
      'node' => $context->data,
    );

    $block = new stdClass();
    $block->title = '';
    $block->content = theme('camp_generic_node_fields', $vars);

    return $block;
  }
}
