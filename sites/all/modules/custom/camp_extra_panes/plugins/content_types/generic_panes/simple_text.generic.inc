<?php

/**
 * @file
 * This file contains a ctools plugin (content-type).
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Simple Text'),
  'description' => t('Simple Text Pane.'),
  'category' => t('Camp: Generic'),
  'edit form' => 'camp_extra_panes_simple_text_edit_form',
  'render callback' => 'camp_extra_panes_simple_text_render',
  'admin info' => 'camp_extra_panes_simple_text_admin_info',
  'defaults' => array(
    'cep_text_value' => array(),
    'cep_text_heading' => 'h4',
    'cep_text_align' => 'center',
  ),
);

/**
 * Edit form for our simple text pane.
 */
function camp_extra_panes_simple_text_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Any HTML or plain text.
  $form['cep_text_value'] = array(
    '#type' => 'text_format',
    '#title' => t('HTML or Simple Text'),
    '#description' => t('If you want to use some HTML (link or video or something else) - feel free to use this field.'),
    '#default_value' => $conf['cep_text_value']['value'],
    '#format' => $conf['cep_text_value']['format'],
  );

  // Customize the touch & feel.
  $form['cep_text_customize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change Touch & Feel'),
    '#description' => t('If you want to customize the touch & feel, see options below.'),
    '#default_value' => $conf['cep_text_customize'],
  );
  $form['cep_text_customize_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Touch & Feel'),
    '#dependency' => array('edit-cep-text-customize' => array(1)),
  );
  $form['cep_text_customize_fieldset']['aligner_start'] = array(
    '#markup' => '<div class="option-text-aligner clearfix">',
  );
  $form['cep_text_customize_fieldset']['cep_text_heading'] = array(
    '#type' => 'select',
    '#title' => t('Heading Tag'),
    '#default_value' => isset($conf['cep_text_heading']) ? $conf['cep_text_heading'] : 'h4',
    '#options' => array(
      'h1' => t('h1'),
      'h2' => t('h2'),
      'h3' => t('h3'),
      'h4' => t('h4'),
      'h5' => t('h5'),
      'h6' => t('h6'),
      'div' => t('div'),
      'span' => t('span'),
    ),
  );
  $form['cep_text_customize_fieldset']['cep_text_align'] = array(
    '#type' => 'select',
    '#title' => t('Text Alignment'),
    '#default_value' => isset($conf['cep_text_align']) ? $conf['cep_text_align'] : 'center',
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
      'justify' => t('Justify'),
    ),
  );
  $form['cep_text_customize_fieldset']['aligner_stop'] = array(
    '#markup' => '</div>',
  );
  return $form;
}


/**
 * Ctools edit form submit handler.
 */
function camp_extra_panes_simple_text_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Rendering of the simple text pane.
 * 
 * Implements a ctools "render callback" of the $plugin.
 */
function camp_extra_panes_simple_text_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $vars = array(
    'text' => $conf['cep_text_value'],
    'heading' => $conf['cep_text_heading'],
    'align' => $conf['cep_text_align'],
  );
  $block->title = '';
  $block->content = theme('cep_simple_text', $vars);

  return $block;
}

/**
 * Ctools "admin info" callback for the $plugin.
 */
function camp_extra_panes_simple_text_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $block->title = t('Simple Text');
  $list = array(
    'type' => 'ul',
    'title' => t('Selected Settings:'),
    'attributes' => array(),
  );
  $list['items'] = _camp_extra_panes_process_conf($conf, '_camp_extra_panes_simple_text_get_settings_value');
  $list_rendered = theme_item_list($list);
  $block->content = $list_rendered;
  return $block;
}

/**
 * Private function that just returns the label of the setting.
 */
function _camp_extra_panes_simple_text_get_settings_value($key, $setting = 'label') {
  $mappings = array(
    'cep_text_value' => array(
      'label' => t('Text'),
      'type' => 'format',
    ),
    'cep_text_heading' => array(
      'label' => t('Heading being used'),
    ),
    'cep_text_align' => array(
      'label' => t('Aligned to'),
    ),
  );
  return isset($mappings[$key][$setting]) ? $mappings[$key][$setting] : FALSE;
}
