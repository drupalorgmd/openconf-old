<?php

/**
 * @file
 * This file contains a ctools plugin (content-type).
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Contact Form w/ Map'),
  'description' => t('A contact form with map pane.'),
  'category' => t('Camp: Generic'),
  'edit form' => 'camp_extra_panes_contact_form_with_map_edit_form',
  'render callback' => 'camp_extra_panes_contact_form_with_map_render',
  'admin info' => 'camp_extra_panes_contact_form_with_map_admin_info',
  'defaults' => array(
    'cep_html_before' => array(),
    'cep_form_title' => '',
    'cep_form_title_heading' => 'h4',
    'cep_html_after' => array(),
  ),
);

/**
 * Edit form for our simple text pane.
 */
function camp_extra_panes_contact_form_with_map_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Any HTML or plain text.

  // Any HTML or plain text.
  $form['cep_html_before'] = array(
    '#type' => 'text_format',
    '#title' => t('HTML or Simple Text: Before the Form'),
    '#description' => t('If you want to use some HTML (link or video or something else) - feel free to use this field.'),
    '#default_value' => $conf['cep_html_before']['value'],
    '#format' => $conf['cep_html_before']['format'],
  );

  // Customize the form title.
  $form['cep_text_customize_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Form Settings'),
  );
  $form['cep_text_customize_fieldset']['aligner_start'] = array(
    '#markup' => '<div class="option-text-aligner clearfix">',
  );
  $form['cep_text_customize_fieldset']['cep_form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Form Title'),
    '#description' => t('Use a title for the contact form.'),
    '#default_value' => $conf['cep_form_title'],
  );
  $form['cep_text_customize_fieldset']['cep_form_title_heading'] = array(
    '#type' => 'select',
    '#title' => t('Heading Tag'),
    '#default_value' => isset($conf['cep_text_heading']) ? $conf['cep_form_title_heading'] : 'h4',
    '#options' => array(
      'h1' => t('h1'),
      'h2' => t('h2'),
      'h3' => t('h3'),
      'h4' => t('h4'),
      'h5' => t('h5'),
      'h6' => t('h6'),
      'div' => t('div'),
      'span' => t('span'),
    ),
  );
  $form['cep_text_customize_fieldset']['aligner_stop'] = array(
    '#markup' => '</div>',
  );

  // Any HTML or plain text.
  $form['cep_html_after'] = array(
    '#type' => 'text_format',
    '#title' => t('HTML or Simple Text: After the Form'),
    '#description' => t('If you want to use some HTML (link or video or something else) - feel free to use this field.'),
    '#default_value' => $conf['cep_html_after']['value'],
    '#format' => $conf['cep_html_after']['format'],
  );
  return $form;
}


/**
 * Ctools edit form submit handler.
 */
function camp_extra_panes_contact_form_with_map_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Rendering of the simple text pane.
 * 
 * Implements a ctools "render callback" of the $plugin.
 */
function camp_extra_panes_contact_form_with_map_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $vars = array(
    'html_before' => $conf['cep_html_before'],
    'title' => $conf['cep_form_title'],
    'title_heading' => $conf['cep_form_title_heading'],
    'html_after' => $conf['cep_html_after'],
  );
  $block->title = '';
  $block->content = theme('cep_contact_form_with_map', $vars);

  return $block;
}

/**
 * Ctools "admin info" callback for the $plugin.
 */
function camp_extra_panes_contact_form_with_map_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $block->title = t('Contact Form w/ MAP:');
  $list = array(
    'type' => 'ul',
    'title' => t('Selected Settings:'),
    'attributes' => array(),
  );
  $list['items'] = _camp_extra_panes_process_conf($conf, '_camp_extra_panes_contact_form_with_map_get_settings_value');
  $list_rendered = theme_item_list($list);
  $block->content = $list_rendered;
  return $block;
}

/**
 * Private function that just returns the label of the setting.
 */
function _camp_extra_panes_contact_form_with_map_get_settings_value($key, $setting = 'label') {
  $mappings = array(
    'cep_html_before' => array(
      'label' => t('Before HTML'),
      'type' => 'format',
    ),
    'cep_form_title' => array(
      'label' => t('Form Title'),
    ),
    'cep_form_title_heading' => array(
      'label' => t('Form Title Heading'),
    ),
    'cep_html_after' => array(
      'label' => t('After HTML'),
      'type' => 'format',
    ),
  );
  return isset($mappings[$key][$setting]) ? $mappings[$key][$setting] : FALSE;
}
