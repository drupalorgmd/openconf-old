<?php

/**
 * @file
 * This file contains a ctools plugin (content-type).
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Breadcrumb'),
  'description' => t('Special Breadcrumb with Title.'),
  'category' => t('Camp: Generic'),
  'icon' => 'icon_page.png',
  'edit form' => 'camp_extra_panes_breadcrumb_edit_form',
  'render callback' => 'camp_extra_panes_breadcrumb_render',
  'admin info' => 'camp_extra_panes_breadcrumb_admin_info',
  'defaults' => array(
    'cep_include_location_date' => FALSE,
    'cep_breadcrumb_date_line' => '',
    'cep_breadcrumb_location_line' => '',
  ),
);

/**
 * Edit form for our simple text pane.
 */
function camp_extra_panes_breadcrumb_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Customize date & location.
  $form['cep_include_location_date'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Date & Location'),
    '#description' => t('Check this box if you want to include the date and location.'),
    '#default_value' => $conf['cep_include_location_date'],
  );
  $form['cep_include_location_date_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Date & Location'),
    '#dependency' => array('edit-cep-include-location-date' => array(1)),
  );
  $form['cep_include_location_date_fieldset']['cep_breadcrumb_date_line'] = array(
    '#type' => 'textfield',
    '#title' => t('Date Line'),
    '#default_value' => $conf['cep_breadcrumb_date_line'],
    '#description' => t('Specify the date line.'),
    '#dependency' => array('edit-cep-include-location-date' => array(1)),
  );
  $form['cep_include_location_date_fieldset']['cep_breadcrumb_location_line'] = array(
    '#type' => 'textfield',
    '#title' => t('Location Line'),
    '#default_value' => $conf['cep_breadcrumb_location_line'],
    '#description' => t('Specify the location line.'),
    '#dependency' => array('edit-cep-include-location-date' => array(1)),
  );

  return $form;
}

/**
 * Edit form for our simple text pane.
 */
function camp_extra_panes_breadcrumb_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Renders the special breadcrumb.
 */
function camp_extra_panes_breadcrumb_render($subtype, $conf, $panel_args) {
  $block = new stdClass();
  $title = drupal_get_title();
  $breadcrumb = drupal_get_breadcrumb();
  $camp_breadcrumb_vars = array(
    'breadcrumb' => $breadcrumb,
    'location' => $conf['cep_breadcrumb_location_line'],
    'date' => $conf['cep_breadcrumb_date_line'],
    'title' => ($title) ? $title : '',
  );
  if (!empty($breadcrumb)) {
    $block->content = theme('cep_breadcrumb', $camp_breadcrumb_vars);
  }
  else {
    $block->content = '';
  }

  return $block;
}

/**
 * Ctools "admin info" callback for the $plugin.
 */
function camp_extra_panes_breadcrumb_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $block->content = t('Special breadcrumb with title.');
  return $block;
}
