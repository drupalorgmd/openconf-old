<?php

/**
 * @file
 * This file contains a ctools plugin (content-type).
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Camp: User Social Links'),
  'description' => t('User Social Links, that contain such links as: Facebook, LinkedIn and Twitter.'),
  'category' => t('User'),
  'edit form' => 'camp_extra_panes_profile_social_links_edit_form',
  'render callback' => 'camp_extra_panes_profile_social_links_render',
  'admin info' => 'camp_extra_panes_profile_social_links_admin_info',
  'required context' => new ctools_context_required(t('User'), 'user'),
  'defaults' => array(),
);

/**
 * Edit form for our pane.
 */
function camp_extra_panes_profile_social_links_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // We'll do the custom form later.

  return $form;
}


/**
 * Ctools edit form submit handler.
 */
function camp_extra_panes_profile_social_links_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Rendering of the user header pane.
 * 
 * Implements a ctools "render callback" of the $plugin.
 */
function camp_extra_panes_profile_social_links_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $account = isset($context->data) ? clone($context->data) : NULL;
  if (!$account) {
    return NULL;
  }

  $block->title = '';
  $block->content = theme('cep_user_profile_social_links', array('account' => $account));

  return $block;
}

/**
 * Ctools "admin info" callback for the $plugin.
 */
function camp_extra_panes_profile_social_links_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $block->title = t('Camp: User Profile Social Links');
  $account_context = array_pop($contexts);
  $list = array(
    'type' => 'ul',
    'title' => t('Selected Settings:'),
    'attributes' => array(),
  );
  $list['items'][] = t('Using argument: @ident', array('@ident' => $account_context->identifier));
  $list_rendered = theme_item_list($list);
  $block->content = $list_rendered;
  return $block;
}
