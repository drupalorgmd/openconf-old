<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('Event Counters'),
  'description' => t('Counts statistics of the camp.'),
  'category' => t('Camp: Launch'),
  'render callback' => 'cep_counters_pane_render',
  'admin info' => 'cep_counters_admin_info',
  'defaults' => array(),
);

/**
 * Ctools render functions.
 */
function cep_counters_pane_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->title = '<none>';

  // Get all the values and render them nicely with a hook_theme function.
  drupal_add_library('waypoints', 'waypoints');
  libraries_load('countUp');
  drupal_add_js(drupal_get_path('module', 'camp_extra_panes') . '/js/cep_counters_init.js');

  $vars = array(
    'header' => '',
    'counters' => array(
      'partners' => array(
        'label' => t('Partners'),
        'value' => _camp_extra_panes_count('partners'),
        'icon-classes' => 'icon ent-light-bulb',
      ),
      'sponsors' => array(
        'label' => t('Sponsors'),
        'value' => _camp_extra_panes_count('sponsors'),
        'icon-classes' => 'icon fui-star-2',
      ),
      'sessions' => array(
        'label' => t('Sessions'),
        'value' => _camp_extra_panes_count('sessions'),
        'icon-classes' => 'icon fui-mic',
      ),
      'attendees' => array(
        'label' => t('Attendees'),
        'value' => _camp_extra_panes_count('attendees'),
        'icon-classes' => 'icon fui-user',
      ),
      'technology' => array(
        'label' => t('Technologies'),
        'value' => _camp_extra_panes_count('technology'),
        'icon-classes' => 'icon fui-gear',
      ),
      'countries' => array(
        'label' => t('Countries'),
        'value' => _camp_extra_panes_count('countries'),
        'icon-classes' => 'icon ent-globe',
      ),
    ),
  );

  $block->content = theme('cep_counters', $vars);
  return $block;
}

/**
 * 'admin info' callback for panel pane.
 */
function cep_counters_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass;
  $block->title = t('Counters');
  $block->content = t('Counters of the event.');
  return $block;
}
