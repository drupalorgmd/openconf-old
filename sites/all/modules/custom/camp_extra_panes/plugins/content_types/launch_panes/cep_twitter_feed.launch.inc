<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('Twitter Feed'),
  'description' => t('Twtr embedded.'),
  'category' => t('Camp: Launch'),
  'render callback' => 'cep_twitter_feed_embed_render',
  'admin info' => 'cep_twitter_feed_admin',
  'defaults' => array(),
);

/**
 * Ctools render functions.
 */
function cep_twitter_feed_embed_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->title = '<none>';
  $block->content = '<a class="twitter-timeline"  href="https://twitter.com/hashtag/opensourcemoldova" data-widget-id="968141088111366145">#opensourcemoldova Tweets</a>';
  $block->content .= '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';

  return $block;
}

/**
 * 'admin info' callback for panel pane.
 */
function cep_twitter_feed_admin($subtype, $conf, $contexts) {
  $block = new stdClass;
  $block->title = t('Frontpage Twtr');
  $block->content = t('Twtr for Open Source Camp Moldova');
  return $block;
}
