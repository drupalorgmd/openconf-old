/**
 * Overall theme functions.
 */
(function($) {
  Drupal.behaviors.cep_counters_init = {
    attach: function(context, settings) {
      var count1 = $(".counter-item.item-1 .value", context).text(),
        count2 = $(".counter-item.item-2 .value", context).text(),
        count3 = $(".counter-item.item-3 .value", context).text(),
        count4 = $(".counter-item.item-4 .value", context).text(),
        count5 = $(".counter-item.item-5 .value", context).text(),
        count6 = $(".counter-item.item-6 .value", context).text();
      $(".counter-item .value", context).text('0');

      var countersLoaded = false;

      $('.counters-wrapper', context).waypoint(function() {
        if (!countersLoaded) {
          var counter1 = new countUp($(".counter-item.item-1 .value", context).get(0), 0, count1, 0, 1.5);
          var counter2 = new countUp($(".counter-item.item-2 .value", context).get(0), 0, count2, 0, 1.5);
          var counter3 = new countUp($(".counter-item.item-3 .value", context).get(0), 0, count3, 0, 1.5);
          var counter4 = new countUp($(".counter-item.item-4 .value", context).get(0), 0, count4, 0, 1.5);
          var counter5 = new countUp($(".counter-item.item-5 .value", context).get(0), 0, count5, 0, 1.5);
          var counter6 = new countUp($(".counter-item.item-6 .value", context).get(0), 0, count6, 0, 1.5);
          setTimeout(function() {
            counter1.start()
          }, 100);
          setTimeout(function() {
            counter2.start()
          }, 200);
          setTimeout(function() {
            counter3.start()
          }, 300);
          setTimeout(function() {
            counter4.start()
          }, 400);
          setTimeout(function() {
              counter5.start()
          }, 500);
          setTimeout(function() {
              counter6.start()
          }, 600);
          countersLoaded = true;
        }
      }, {
        offset: '50%'
      });

    }
  }
})(jQuery);