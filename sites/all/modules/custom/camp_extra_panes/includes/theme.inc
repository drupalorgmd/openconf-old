<?php

/**
 * @file
 * This file contains all the theming-related and template-related functions.
 * Template, preprocess & process functions live here as well.
 */

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_cep_multicolor_header(&$vars) {
  // Prepare the background image.
  if ($vars['image'] !== 0) {
    $file = file_load($vars['image']);

    if ($file) {
      // Check if we want to apply an image style.
      if (!empty($vars['image_style'])) {
        $image_style_vars = array(
          'style_name' => $vars['image_style'],
          'path' => $file->uri,
          'alt' => '',
          'title' => '',
          'attributes' => array(
            'class' => array('background-image'),
          ),
        );
        $vars['image'] = theme('image_style', $image_style_vars);
      }
      // Otherwise just render the original image.
      else {
        $image_vars = array(
          'path' => $file->uri,
          'alt' => '',
          'title' => '',
          'width' => $file->metadata['width'],
          'height' => $file->metadata['height'],
          'attributes' => array(
            'class' => array('background-image'),
          ),
        );
        $vars['image'] = theme('image', $image_vars);
      }
    }
  }
  else {
    $vars['image'] = FALSE;
  }

  // Preprocess logo.
  if (isset($vars['include_logo']) && $vars['include_logo']) {
    $logo = theme_get_setting('logo');
    $logo_rendered = theme('image', array('path' => $logo, 'attributes' => array('class' => array('multicolor-logo'))));
    // Now check the link.
    if ($vars['logo_link']) {
      $vars['logo'] = l($logo_rendered, '<front>', array('html' => TRUE));
    }
    else {
      $vars['logo'] = $logo_rendered;
    }
  }
  else {
    $vars['include_logo'] = FALSE;
  }

  if (isset($vars['mission']) && $vars['mission']) {
    $vars['mission'] = variable_get('site_slogan', '');
  }
  else {
    $vars['mission'] = FALSE;
  }
  if (isset($vars['header_description']) && !empty($vars['header_description'])){
    $vars['header_description'] = check_markup($vars['header_description']['value'], $vars['header_description']['format']);
  }
  if (isset($vars['extra_html']) && !empty($vars['extra_html'])) {
    $vars['extra_html'] = check_markup($vars['extra_html']['value'], $vars['extra_html']['format']);
  }
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_cep_simple_text(&$vars) {
  $vars['output'] = theme('html_tag', array(
    'element' => array(
      '#tag' => $vars['heading'],
      '#attributes' => array(
        'class' => array('simple-text', 'text-' . $vars['align']),
      ),
      '#value' => check_markup($vars['text']['value'], $vars['text']['format']),
    ),
  ));
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_cep_contact_form_with_map(&$vars) {
  module_load_include('inc', 'contact', 'contact.pages');
  $contact_form = drupal_get_form('contact_site_form');
  $form_title = theme('html_tag', array(
    'element' => array(
      '#tag' => $vars['title_heading'],
      '#attributes' => array(),
      '#value' => $vars['title'],
    ),
  ));
  $contact_form['#prefix'] = $form_title;
  $vars['contact_form'] = $contact_form;

  $vars['html_before'] = check_markup($vars['html_before']['value'], $vars['html_before']['format']);
  $vars['html_after'] = check_markup($vars['html_after']['value'], $vars['html_after']['format']);
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_cep_breadcrumb(&$vars) {
  $vars['breadcrumb'] = theme('breadcrumb', array('breadcrumb' => $vars['breadcrumb']));
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_sponsorship_level(&$vars) {
  $vars['output'] = theme('html_tag', array(
    'element' => array(
      '#tag' => $vars['heading'],
      '#attributes' => array(
        'class' => array('simple-text', 'text-' . $vars['align']),
      ),
      '#value' => $vars['text'],
    ),
  ));
}

function template_preprocess_cep_user_profile_header(&$vars) {
  $account = $vars['account'];
  $account_emw = entity_metadata_wrapper('user', $account);
  $vars['full_name'] = t('@first @last (@nick)', array(
    '@first' => $account_emw->field_name_first->value(),
    '@last' => $account_emw->field_name_last->value(),
    '@nick' => $account->name,
  ));
  // If both are defined.
  if ($account_emw->__isset('field_profile_org') && $account_emw->field_profile_org->getIdentifier()) {
    if ($account_emw->__isset('field_profile_job_title') && $account_emw->field_profile_job_title->getIdentifier()) {
      $vars['job_title'] = t('@job at @organization', array(
        '@job' => $account_emw->field_profile_job_title->name->value(),
        '@organization' => $account_emw->field_profile_org->name->value(),
      ));
    }
    else {
      $vars['job_title'] = t('Works at @organization', array(
        '@organization' => $account_emw->field_profile_org->name->value(),
      ));
    }
  }
  else {
    if ($account_emw->__isset('field_profile_job_title') && $account_emw->field_profile_job_title->getIdentifier()) {
      $vars['job_title'] = t('Works as @job', array(
        '@job' => $account_emw->field_profile_job_title->name->value(),
      ));
    }
    else {
      $vars['job_title'] = '';
    }
  }
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_cep_user_profile_social_links(&$vars) {
  $account = $vars['account'];
  $account_emw = entity_metadata_wrapper('user', $account);
  $item_list = array();

  $field_facebook_items = field_get_items('user', $account, 'field_facebook');
  $field_facebook_arr = field_view_value('user', $account, 'field_facebook', $field_facebook_items[0]);
  $field_facebook_arr['#element']['attributes']['class'] = 'pis-facebook';
  $item_list[] = drupal_render($field_facebook_arr);

  $field_twitter_items = field_get_items('user', $account, 'field_twitter');
  $field_twitter_arr = field_view_value('user', $account, 'field_twitter', $field_twitter_items[0]);
  $field_twitter_arr['#element']['attributes']['class'] = 'pis-twitter';
  $item_list[] = drupal_render($field_twitter_arr);

  $field_linkedin_items = field_get_items('user', $account, 'field_linkedin');
  $field_linkedin_arr = field_view_value('user', $account, 'field_linkedin', $field_linkedin_items[0]);
  $field_linkedin_arr['#element']['attributes']['class'] = 'pis-linkedin';

  $item_list_vars = array(
    'type' => 'ul',
    'title' => '',
    'items' => $item_list,
  );
  $vars['social_links'] = theme('item_list', $item_list_vars);
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_cep_session_header(&$vars) {
  $fields_icons = array(
    'field_session_status' => 'ent-switch',
    'field_session_language' => 'ent-pencil',
    'field_session_difficulty' => 'ent-medal',
    'field_session_track' => 'ent-flow-parallel',
    'field_room' => 'ent-location',
    'field_speaker_s_' => 'ent-megaphone',
    'field_time_slot' => 'ent-time-slot',
  );
  $fields_settings = array(
    'field_speaker_s_' => array('link' => TRUE),
  );

  $items = array();
  foreach ($fields_icons as $field_name => $icon_class) {
    if (isset($vars['node']->$field_name)) {
      if (isset($fields_settings[$field_name])) {
        $field_view = field_view_field('node', $vars['node'], $field_name, array('settings' => $fields_settings[$field_name]));
      }
      else {
        $field_view = field_view_field('node', $vars['node'], $field_name);
      }
      if (!empty($field_view)) {
        $items[] = '<div class="field-wrapper-with-icon"><div class="icon ' . $icon_class . '"></div>'. render($field_view) . '</div>';
      }
    }
  }
  $item_list_vars = array(
    'type' => 'ul',
    'title' => '',
    'items' => $items,
  );
  $vars['session_header_items'] = theme('item_list', $item_list_vars);
}
