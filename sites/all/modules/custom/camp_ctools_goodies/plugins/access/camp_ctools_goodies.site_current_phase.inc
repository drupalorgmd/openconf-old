<?php

/**
 * @file
 * Plugin to provide access control/visibility based on existence of a specified context
 */

$plugin = array(
  'title' => t("Camp: Current Site Phase"),
  'description' => t('Control access by whether or not a context exists and contains data.'),
  'callback' => 'camp_ctools_goodies_site_current_phase_check',
  'settings form' => 'camp_ctools_goodies_site_current_phase_setting_form',
  'summary' => 'camp_ctools_goodies_site_current_phase_summary',
  'defaults' => array('match_phase' => CAMP_CORE_PHASE_ANNOUNCEMENT),
);

/**
 * Check for access.
 */
function camp_ctools_goodies_site_current_phase_check($conf){
  $current_phase = variable_get('camp_core_current_phase', CAMP_CORE_PHASE_ANNOUNCEMENT);
  if ($current_phase == $conf['match_phase']) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Plugin's settings form.
 */
function camp_ctools_goodies_site_current_phase_setting_form($form, &$form_state, $conf) {
  $form['settings']['match_phase'] = array(
    '#type' => 'radios',
    '#title' => t('Check if current phase is:'),
    '#options' => _camp_core_get_phases_options(),
    '#default_value' => $conf['match_phase'],
    '#description' => t('Check if the current phase of the website matches one of these.'),
  );
  return $form;
}

/**
 * Access plugin's summary.
 */
function camp_ctools_goodies_site_current_phase_summary($conf) {
  return t('Check if current phase is !phase', array('!phase' => _camp_core_get_phases_options($conf['match_phase'])));
}
