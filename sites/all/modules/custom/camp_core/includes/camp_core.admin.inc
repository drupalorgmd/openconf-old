<?php

/**
 * @file
 * Contains all of the admin pages functionality: forms, validations, etc.
 */

/**
 * Callback that returns system settings page.
 */
function camp_core_system_settings_page() {
  $block['content']['#markup'] = 'Not done yet, chillax.';
  return $block;
}

/**
 * Callback that builds the phases settings form renderable array.
 */
function camp_core_phases_settings_form($form, &$form_state) {
  $form = array();
  $form['camp_core_current_phase'] = array(
    '#type' => 'radios',
    '#title' => t('Current Phase', array(), array('context' => 'Camp: Dashboards')),
    '#default_value' => variable_get('camp_core_current_phase', CAMP_CORE_PHASE_ANNOUNCEMENT),
    '#options' => _camp_core_get_phases_options(),
    '#description' => t('Please select the <strong>current phase</strong> of the website. Make sure you have everything set-up for this transition (all the panels and pages, etc.).'),
  );
  $form = system_settings_form($form);
  return $form;
}

/**
 * Helper function that returns options for existing phases.
 *
 * @param null $key
 *   Key, if it's set, we return the label of that key, otherwise, we return all phases.
 *
 * @return array
 *   Either one label, or all labels.
 */
function _camp_core_get_phases_options($key = NULL) {
  $phases = array();
  $phases[CAMP_CORE_PHASE_ANNOUNCEMENT] = t('Announcement Phase', array(), array('context' => 'Camp: Dashboards'));
  $phases[CAMP_CORE_PHASE_LAUNCHED] = t('Launched Phase', array(), array('context' => 'Camp: Dashboards'));
  $phases[CAMP_CORE_PHASE_LAUNCHED_SCHEDULE] = t('Launched with Schedule Phase', array(), array('context' => 'Camp: Dashboards'));
  $phases[CAMP_CORE_PHASE_POST_LAUNCH] = t('Post-Launch Phase', array(), array('context' => 'Camp: Dashboards'));
  return isset($key) ? $phases[$key] : $phases;
}
