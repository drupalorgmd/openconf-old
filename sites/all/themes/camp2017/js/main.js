(function ($) {

	'use strict';
  /**
   * All camp-theme related functionality goes here.
   */
   Drupal.behaviors.campTheme = {
   	attach: function (context, settings) {
      // Responsive Font-sizes
      $('.header-text', context).fitText(1.1, {minFontSize: '22px', maxFontSize: '67px'});
      $('.cta-top-wrapper .cta-title', context).fitText(1.5, {minFontSize: '18px', maxFontSize: '36px'});
      $('.cta-left-wrapper .cta-title', context).fitText(1.5, {minFontSize: '36px', maxFontSize: '48px'});
      $('.simple-text ', context).fitText(1.5, {minFontSize: '18px', maxFontSize: '24px'});
      $('.frame-previous_results .pane-title', context).fitText(1.5, {minFontSize: '36px', maxFontSize: '48px'});
      $('.mission', context).fitText(1.5, {minFontSize: '20px', maxFontSize: '24px'});
      $('.node-type-news .frame-content .pane-title', context).fitText(0, {minFontSize: '26px', maxFontSize: '36px'});
      $('.not-front .frame-content .pane-node-comment-form .pane-title', context).fitText(0, {maxFontSize: '12px'});
      $('.page-sponsors .frame-content .pane-title', context).fitText(1.5, {minFontSize: '14px', maxFontSize: '48px'});
      $('.node-type-job .panel-pane h2', context).fitText(1, {minFontSize: '14px', maxFontSize: '18px'});
      $('.node-type-job .pane-node-title .pane-content h2', context).fitText(1.5, {minFontSize: '24px', maxFontSize: '36px'});
      // Enable the bootstrap tooltips.
      $('[data-toggle="tooltip"]', context).tooltip();
    }
  };

 // /**
 //   * All camp-theme related functionality goes here.
 //   */
 //  Drupal.behaviors.campTheme = {

 //    attach: function (context, settings) {
 //      // Enable the bootstrap tooltips.
 //      $('[data-toggle="tooltip"]', context).tooltip();
 // 
 //      // Apply the one-page inner links to smooth transition.
 //      $('a.one-page-link').click(function(){
 //        var offset = 0;
 //        offset += $(this).hasClass('offset-100') ? 100 : 0;
 //        offset += $(this).hasClass('offset-200') ? 200 : 0;
 //        $('html, body').animate({
 //          scrollTop: $(this.hash).offset().top - offset
 //        }, 800);
 //        return false;
 //      });

 //      $('#edit-subject').on('focus', function() {
 //        $('#edit-message').show();
 //      });
 //    }
 //  };

  // Slider for three column.
  Drupal.behaviors.sessionsSlider = {
    attach: function (context, settings) {
      var $sliderContainer = $('.frame-submitted_sessions .view-content, .view-featured-speakers > .view-content');
      $sliderContainer.slick({
        dots: false,
        infinite: true,
        speed: 300,
        arrows: true,
        mobileFirst: true,
        draggable: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
        ]

      });
      $('.slick-slider-wrapper .field-items').slick({
        dots: false,
        infinite: true,
        speed: 300,
        arrows: true,
        mobileFirst: true,
        draggable: false,
        slidesToShow: 1,
        slidesToScroll: 1
      });
    }
  };

  Drupal.behaviors.chosenInit = {
   attach: function (context, settings) {
    $('select').chosen();
  }
};

Drupal.behaviors.mobileMenu = {
	attach: function(context, settings) {
		Drupal.MoldCamp.responsiveMenu($('.menu-block-ctools-main-menu-1 > .menu-main-menu', context), {
			"widthKickIn": 765,
			"navigationTitle": Drupal.t('Tap Menu')
		});
	}
};

Drupal.MoldCamp = {
	responsiveMenu: function($menu, settings) {
		$menu.before($('<div class="tap-menu">' + settings.navigationTitle + '</div>'));

		$('.tap-menu').click(function() {
			$menu.toggle();
		});

		$(window).bind('resize', {
			'menu': $menu,
			'settings': settings
		}, Drupal.MoldCamp.responsiveCheck);
		$(window).trigger('resize');
	},

	responsiveCheck: function(evObj) {
		var $menu = evObj.data.menu,
		settings = evObj.data.settings,
		$mobileMenu = $('.tap-menu');

		var windowWidth = $(window).width();
		if (windowWidth <= settings.widthKickIn) {
			$mobileMenu.show().addClass('visible');
			$menu.hide().addClass('mobile-enabled');
			$('.depth-1 > a', $menu).bind('click', Drupal.MoldCamp.levelOneHanler);
		} else {
			$mobileMenu.hide().removeClass('visible');
			$menu.show().removeClass('mobile-enabled');
			$('.depth-1 > a', $menu).unbind('click', Drupal.MoldCamp.levelOneHanler);
		}
	},

	levelOneHanler: function(evObj) {
		if (!$(this).parent().hasClass('clicked-once')) {
			evObj.preventDefault();
			$(this).parent().addClass('clicked-once');
		} else {
			return;
		}
	}
};
})(jQuery);
