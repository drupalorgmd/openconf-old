<?php
/**
 * @file
 * Add custom class to pane title and pane content.
 */

// Plugin definition.
$plugin = array(
  'title' => '--> ' . t('Moldcamp: Slider'),
  'description' => t('Make slider from block of images'),
  'pane settings form' => 'moldcamp_pane_slider_settings_form',
  'render pane' => 'moldcamp_pane_slider',
);

/**
 * Render callback for a single pane.
 */
function theme_moldcamp_pane_slider($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];
  $content_class = isset($vars['settings']['content_class']) ? $vars['settings']['content_class'] : '';
  $content_class .= ' slick-slider-wrapper';
  if (empty($content->content)) {
    return;
  }

  $output = theme('panels_pane', array(
    'content' => $content,
    'pane' => $pane,
    'display' => $display,
    'content_class' => $content_class,
  ));
  return $output;
}

/**
 * Panetitle plugin settings.
 */
function moldcamp_pane_slider_settings_form($style_settings, $display, $pid, $type, $form_state) {
  $form['content_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Content class'),
    '#default_value' => (isset($style_settings['content_class'])) ? $style_settings['content_class'] : '',
    '#description' => t('Add an additional class to pane content wrapper'),
  );
  return $form;
}
