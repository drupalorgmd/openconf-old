<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$circle_path = drupal_get_path('theme', 'circle');
include_once './' . $circle_path . '/plugins/layouts/bootstrap-settings.inc';

$plugin = array(
  'title' => t('Two Column (clear)'),
  'category' => t('MC Columns'),
  'icon' => 'two-col.png',
  'theme' => 'two-col',
  'css' => 'two-col.css',
  'settings' => array('default_behavior' => NULL, 'use_container' => NULL, 'panel_class' => NULL),
  'settings form' => '_circle_bootstrap_layout_settings_form',
  'regions' => array(
    'colone'=>t('Left col content'),
    'coltwo'=>t('Right col content'),
  )
);






