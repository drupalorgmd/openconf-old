<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$circle_path = drupal_get_path('theme', 'circle');
include_once './' . $circle_path . '/plugins/layouts/bootstrap-settings.inc';

$plugin = array(
  'title' => t('Two Columns (content)'),
  'category' => t('MC Columns'),
  'icon' => 'two-col-content.png',
  'theme' => 'two-col-content',
  'css' => 'two-col-content.css',
  'settings' => array('default_behavior' => NULL, 'use_container' => NULL, 'panel_class' => NULL),
  'settings form' => '_circle_bootstrap_layout_settings_form',
  'regions' => array(
    'sidebar' => t('Left Sidebar'),
    'content' => t('Content'),
  )
);
