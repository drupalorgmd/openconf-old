<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a very simple "sidebar column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to sidebar
 *   panel of the layout. This layout supports the following sections:
 *   $content['sidebar']: First column
 *   $content['content']: Second column.
 */
?>
<?php $panel_class = check_plain((isset($settings['panel_class']) && $settings['panel_class']) ? $settings['panel_class'] : ''); ?>
<div class="panel-content-column clearfix container-fluid <?php print $panel_class; ?>" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="container">
    <div class="row">
      <?php if (isset($content['sidebar']) && $content['sidebar']): ?>
        <div class="left-sidebar-column">
          <?php print $content['sidebar']; ?>
        </div>
      <?php endif; ?>
      <?php if (isset($content['content']) && $content['content']): ?>
        <div class="content-column">
          <?php print $content['content']; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>