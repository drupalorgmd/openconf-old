<?php
/**
 * @file
 * Template for a 1 column panel layout.
 *
 * This template provides a very simple "one column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */
?>
<div class="simple-page-layout">
	<div class="container clearfix simple-section-top">
	<?php if ($content['colone']):?>
	  <div class="col-one">
	    <?php print $content['colone']; ?>
	  </div>
	<?php endif ?>
	<?php if ($content['coltwo']):?>
	  <div class="col-two">
	    <?php print $content['coltwo']; ?>
	  </div>
	<?php endif ?>
	<?php if ($content['colthree']):?>
	  <div class="col-three">
	    <?php print $content['colthree']; ?>
	  </div>
	<?php endif ?>
	</div>
	<div class="container clearfix simple-section-middle">
	<?php if ($content['colfour']):?>
	  <div class="col-colfour">
	    <?php print $content['colfour']; ?>
	  </div>
	<?php endif ?>
	</div>
	<div class="container clearfix simple-section-bottom">
	<?php if ($content['colfive']):?>
	  <div class="col-colfive">
	    <?php print $content['colfive']; ?>
	  </div>
	<?php endif ?>
	</div>
</div>
