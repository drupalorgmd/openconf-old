<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$circle_path = drupal_get_path('theme', 'circle');
include_once './' . $circle_path . '/plugins/layouts/bootstrap-settings.inc';

$plugin = array(
  'title' => t('Simple page'),
  'category' => t('MC Columns'),
  'icon' => 'simple-page.png',
  'theme' => 'simple-page',
  'css' => 'simple-page.css',
  'settings' => array('default_behavior' => NULL, 'use_container' => NULL, 'panel_class' => NULL),
  'settings form' => '_circle_bootstrap_layout_settings_form',
  'regions' => array(
    'colone'=>t('Left col (top)'),
    'coltwo'=>t('Center Content'),
    'colthree'=>t('Right col (top)'),
    'colfour'=>t('Middle section'),
    'colfive'=>t('Footer'),
  )
);






