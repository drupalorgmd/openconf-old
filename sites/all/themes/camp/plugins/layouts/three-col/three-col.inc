<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$circle_path = drupal_get_path('theme', 'circle');
include_once './' . $circle_path . '/plugins/layouts/bootstrap-settings.inc';

$plugin = array(
  'title' => t('Three Column (clear)'),
  'category' => t('MC Columns'),
  'icon' => 'three-col.png',
  'theme' => 'three-col',
  'css' => 'three-col.css',
  'settings' => array('default_behavior' => NULL, 'use_container' => NULL, 'panel_class' => NULL),
  'settings form' => '_circle_bootstrap_layout_settings_form',
  'regions' => array(
    'colone'=>t('Left col content'),
    'coltwo'=>t('Center col content'),
    'colthree'=>t('Right col content'),
  )
);






