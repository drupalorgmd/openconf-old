<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$circle_path = drupal_get_path('theme', 'circle');
include_once './' . $circle_path . '/plugins/layouts/bootstrap-settings.inc';

$plugin = array(
  'title' => t('One Column (clear)'),
  'category' => t('MC Columns'),
  'icon' => 'one-col.png',
  'theme' => 'one-col',
  'css' => 'one-col.css',
  'settings' => array('default_behavior' => NULL, 'use_container' => NULL, 'panel_class' => NULL),
  'settings form' => '_circle_bootstrap_layout_settings_form',
  'regions' => array(
    'onecol'=>t('One col content'),
  )
);






