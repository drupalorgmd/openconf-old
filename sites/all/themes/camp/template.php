<?php

/**
 * @file
 * All the good stuff lives here.
 */

/**
 * Implements hook_preprocess_node.
 */
function camp_preprocess_node(&$vars) {
  if (isset($vars['view_mode']) && isset($vars['type'])) {
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'] . '__' . $vars['view_mode'];
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function camp_preprocess_fieldable_panels_pane(&$vars) {
  if (isset($vars['elements']['#view_mode']) && isset($vars['elements']['#bundle'])) {
    $vars['theme_hook_suggestions'][] = 'fieldable_panels_pane__' . $vars['elements']['#bundle'] . '__' . $vars['elements']['#view_mode'];
  }
}

/**
 * Implements template_preprocess_entity().
 */
function camp_preprocess_entity(&$variables, $hook) {
  $function = 'camp_preprocess_' . $variables['entity_type'];
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}

/**
 * Field Collection-specific implementation of template_preprocess_entity().
 */
function camp_preprocess_field_collection_item(&$variables) {
  if (($variables['elements']['#bundle'] == 'field_url_with_attributes') && (!empty($variables['field_collection_item']->field_class))) {
    $variables['content']['field_url'][0]['#element']['attributes']['class'] = $variables['field_collection_item']->field_class['und'][0]['value'];
  }
}
