<div class="cta-1-column <?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_suffix); ?>
  <div class="cta-top-wrapper">
    <div class="cta-title"><?php print $content['title']['#value']; ?></div>
  </div>
  <div class="cta-bottom-wrapper">
    <?php print render($content['field_url_with_attributes']); ?>
    <div class="cta-description"><?php print render($content['field_description']); ?></div>
  </div>
</div>
