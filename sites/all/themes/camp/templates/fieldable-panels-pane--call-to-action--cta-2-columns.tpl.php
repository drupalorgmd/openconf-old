<div class="cta-2-columns <?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_suffix); ?>
  <div class="cta-left-wrapper col-md-9">
    <div class="cta-title"><?php print $content['title']['#value']; ?></div>
    <div class="cta-description"><?php print render($content['field_description']); ?></div>
  </div>
 <div class="cta-right-wrapper col-md-3">
    <?php print render($content['field_url_with_attributes']); ?>
 </div>
</div>
