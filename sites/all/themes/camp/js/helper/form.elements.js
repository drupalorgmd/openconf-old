/**
 * @file
 *
 * Simple Elements - Material Designish (inspired by) form elements.
 *
 * Created by Nikro (nikro.md@gmail.com);
 */
(function($) {
    // Declaring our custom behaviors.
    $.fn.applySE = function() {
        return this.each(function() {
            var $this = $(this),
                floatingLabel = $('<label class="floating-label" for="' + this.id + '">' + $this.attr('placeholder') + '</label>');
            $this.removeAttr('placeholder');
            // Make a correct structure.
            var hiddenClass = ($this.hasClass('hidden')) ? ' hidden' : '';
            $this.removeClass(hiddenClass);

            // We have to check if there's already a declared form-group as parent.
            $this.wrap('<div class="se-floating-label-wrapper form-group' + hiddenClass + '"/>').before(floatingLabel);

            // Animate the label.
            $this.on('focus', function() {
                floatingLabel.addClass('floated');
            });
            $this.on('focusout', function() {
                if ($(this).val() == '') {
                    floatingLabel.removeClass('floated');
                }
            });
        });
    };
})(jQuery);
