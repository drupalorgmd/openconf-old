<?php

/**
 * @file
 * A drupal template file.
 * 
 * Rendering template for the multi-colored header with logo.
 */

?>

<div class="multi-header-wrapper">
  <div class="multicolor-layer">
  <?php if ($image): ?>
    <div class="background-image">
      <div class="earth-wrapper">
        <div class="earth"></div>
      </div>
      <div class="cosmic-wrapper">
        <div class="cosmic-meteor"></div>
      </div>
      <?php print $image; ?>
    </div>
  <?php endif; ?>
    <div class="container">
      <div class="multicolor-left-wrapper">
        <?php if ($include_logo): ?>
          <?php print $logo; ?>
        <?php endif; ?>

        <?php if ($mission): ?>
          <p class="mission"><?php print $mission; ?></p>
        <?php endif; ?>

        <?php if ($header_text): ?>
          <h1><?php print $header_text; ?></h1>
        <?php endif; ?>

        <?php if ($header_description): ?>
          <div class="header-description-wrapper">
            <?php print $header_description; ?>
          </div>
        <?php endif; ?>

        <?php if ($extra_html): ?>
          <?php print $extra_html; ?>
        <?php endif; ?>
      </div>

      <?php if ($include_event_date): ?>
        <div class="date-location-wrapper">
          <div class="date-location-inner">
            <div class="date announce-row"><span class="fui-location"></span><?php print $event_location; ?></div>
            <div class="location announce-row"><?php print $event_date; ?></div>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
