# OpenConf - Drupal 7 based Open Conference Distro

## Intro

This is a Drupal 7 Distribution, developed at Drupal Moldova Association with the support of USAID and the Government of Sweden. We used this platfor for our events such as: Moldcamp 2017 - DrupalCamp in Moldova, and Open Source Camp Moldvoa 2018.

## Who should use it?

This was intended to be used for Open Conferences - an event where any attendee can become a speaker by simply submitting a session. Once all sessions are submitted, Staff decides which to accept and which to refuse, creates a schedule and in a couple of weeks the event takes place.

## Who do I need, to implement a platform?

Although most of the heavylifting is already done, you'll still need a Drupal developer to place all the things together and adjust the platform to the conferences needs. Also, because you'll want to adjust the visuals of the website, most probably you will also need a Front-end developer as well.

# Documentation

* Technical Document (Romanian): https://docs.google.com/document/d/19xcrA97WeN9TvZ0maMwKCIqzbWOwFXGkzEvyMH7ESUo/edit#heading=h.dic3jlfwjgn8

